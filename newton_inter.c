#include <newton_inter.h>
#include <stdio.h>

void _gen_diff_table() {
    for(int j = 0; newt.n > j; j++) {
        newt.diff_table[0][j] = newt.y[j];
    }
    for(int i = 1; newt.n > i; i++) {
        for (int j = 0; newt.n - i > j; j++) {
            newt.diff_table[i][j] = (newt.diff_table[i - 1][j + 1] - newt.diff_table[i - 1][j]) / (newt.x[j + i] - newt.x[j]);       
        }
    }
}

void newton_inter_init(double* x, double* y, int n) {
    newt.x = x;
    newt.y = y;
    newt.n = n;
    _gen_diff_table();
}

void newton_inter_print() {
    double b;
    for(int i = 0; newt.n > i; i++) {
        b = newt.diff_table[i][0];
        if(b != 0.0) {
            if(i != 0) printf(" + ");
            printf("%f", b);
            for(int j = 0; i > j; j++) {
                printf("(x - %f)", newt.x[j]);
            }
        }
    }
    printf("\n");
}

double newton_inter_eval(double x) {
    double sum = 0.0;
    double pre_sum = 1;
    sum = newt.diff_table[0][0];
    for(int i = 1; newt.n > i; i++) {
        pre_sum *= (x - newt.x[i - 1]);
        sum += pre_sum * newt.diff_table[i][0];
    }
    return sum;
}