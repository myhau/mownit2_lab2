#ifndef _INTER_TEST_
#define _INTER_TEST_

#include <time.h>

#define INTER_LAGRANGE 1
#define INTER_NEWTON 2
#define INTER_GSL 3

clock_t start;

typedef double (*f_double)(double);

void generate_n(double* x_alloc, double* y_alloc, int n, double from, double to);

void sample_f(f_double f, double* x_alloc, double* y_alloc, int n, double from_x, double to_x);

void gen_inter_random_file(const char* fle_inter, const char* fle_points, int n, int n_sample, int inter_type, double from_x, double to_x);

void gen_inter_file(const char* fle_inter, const char* fle_points, double* x, double* y, int n, int n_sample, int inter_type, double from_x, double from_y);

void gen_times_interp();

void gen_times_eval();

void time_init();

double time_diff();

#endif