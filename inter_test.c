#include <inter_test.h>
#include <lagrange_inter.h>
#include <newton_inter.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_interp.h>
#include <math.h>


void generate_n(double* x_alloc, double* y_alloc, int n, double from, double to) {
    int N2 = 1000000;
    while(n) {
        x_alloc[--n] = ((rand() % N2) / (double)N2) * (to - from) + to;
        y_alloc[n] = ((rand() % N2) / (double)N2) * (to - from) + to;
    }
}

void sample_f(f_double f, double* x_alloc, double* y_alloc, int n, double from_x, double to_x) {
    double step = (to_x - from_x) / (double)(n - 1);
    for(int i = 0; n > i; i++) {
        x_alloc[i] = from_x + (double)i * step;
        y_alloc[i] = f(x_alloc[i]);
    }
}

void gen_inter_random_file(const char* fle_inter, const char* fle_points, int n, int n_sample, int inter_type, double from_x, double to_x) {
    double* x = malloc(n * sizeof(double));
    double* y = malloc(n * sizeof(double));
    generate_n(x, y, n, 0.0, 10000.0);
    gen_inter_file(fle_points, fle_inter, x, y, n, n_sample, inter_type, from_x, to_x);
    free(x);
    free(y);
}

void gen_inter_file(const char* fle_inter, const char* fle_points, double* x, double* y, int n, int n_sample, int inter_type, double from_x, double to_x) {

    double from = from_x;
    double to = to_x;
    double step = (to_x - from_x) / (double)n_sample;

    FILE* file_inter = fopen(fle_inter, "w");
    FILE* file_points = fopen(fle_points, "w");

    for(int i = 0; n > i; i++) {
        fprintf(file_points, "%f %f\n", x[i], y[i]);
    }
    
    gsl_interp_accel *acc;
    gsl_spline* spline;
    switch(inter_type) {
        case INTER_NEWTON:
            newton_inter_init(x, y, n);
            for(double i = from; to > i; i += step) {
                fprintf(file_inter, "%f %f\n", i, newton_inter_eval(i));
            }
            break;
        case INTER_LAGRANGE:
            lagrange_inter_init(x, y, n);
            for(double i = from; to > i; i += step) {
                fprintf(file_inter, "%f %f\n", i, lagrange_inter_eval(i));
            }
            break;
        case INTER_GSL:
            acc = gsl_interp_accel_alloc();
            spline = gsl_spline_alloc(gsl_interp_polynomial, n);

            gsl_spline_init(spline, x, y, n);
            for (double i = from; to > i; i += step) {
                double val = gsl_spline_eval(spline, i, acc);
                fprintf(file_inter, "%f %f\n", i, val);
            }
            gsl_spline_free(spline);
            gsl_interp_accel_free(acc);
            break;
    }

    fclose(file_points);
    fclose(file_inter);
}


void time_init() {
    start = clock();
}

double time_diff() {
    clock_t now_c = clock();

    double ret = (now_c - start) / (double)CLOCKS_PER_SEC;
    start = now_c;
    return ret;
}

// measures only evaluation times
void gen_times_eval() {
    int max_n = 100;

    double from = 0.0;

    double to = 20.0;

    int n_samples = 10000;

    double step = (to - from)/n_samples;

    const char* lagr_name = "time_eval_lagr.txt";
    const char* gsl_name = "time_eval_gsl.txt";
    const char* newt_name = "time_eval_newt.txt";

    FILE* lagr_times = fopen(lagr_name, "w");
    FILE* gsl_times = fopen(gsl_name, "w");
    FILE* newt_times = fopen(newt_name, "w");
    
    gsl_interp_accel *acc;
    gsl_spline* spline;



    for(int n = 3; max_n > n; n++) {
        double* x = (double*)malloc(n * sizeof(double));
        double* y = (double*)malloc(n * sizeof(double));
        sample_f(sin, x, y, n, from, to);
        double lagr_time;
        double gsl_time;
        double newt_time;

        // NEWTON
        newton_inter_init(x, y, n);
        time_init();
        for(double i = from; to > i; i += step) {
            newton_inter_eval(i);
        }
        newt_time = time_diff();

        // LAGRANGE
        lagrange_inter_init(x, y, n);
        time_init();
        for(double i = from; to > i; i += step) {
            lagrange_inter_eval(i);
        }
        lagr_time = time_diff();

        // GSL
        acc = gsl_interp_accel_alloc();
        
        spline = gsl_spline_alloc(gsl_interp_polynomial, n);
        gsl_spline_init(spline, x, y, n);

        time_init();
        for (double i = from; to > i; i += step) {
            double val = gsl_spline_eval(spline, i, acc);
        }
        gsl_time = time_diff();
        gsl_spline_free(spline);
        gsl_interp_accel_free(acc);

        fprintf(lagr_times, "%f %f\n", (double)n, lagr_time);
        fprintf(newt_times, "%f %f\n", (double)n, newt_time);
        fprintf(gsl_times, "%f %f\n", (double)n, gsl_time);

        free(x);
        free(y);
    }


    fclose(lagr_times);
    fclose(gsl_times);
    fclose(newt_times);

}


void gen_times_interp() {
   int max_n = 100;

    double from = 0.0;

    double to = 20.0;

    int n_samples = 10000;

    double step = (to - from)/n_samples;

    const char* lagr_name = "time_interp_lagr.txt";
    const char* gsl_name = "time_interp_gsl.txt";
    const char* newt_name = "time_interp_newt.txt";

    FILE* lagr_times = fopen(lagr_name, "w");
    FILE* gsl_times = fopen(gsl_name, "w");
    FILE* newt_times = fopen(newt_name, "w");
    
    gsl_interp_accel *acc;
    gsl_spline* spline;



    for(int n = 3; max_n > n; n++) {
        double* x = (double*)malloc(n * sizeof(double));
        double* y = (double*)malloc(n * sizeof(double));
        sample_f(sin, x, y, n, from, to);
        double lagr_time;
        double gsl_time;
        double newt_time;

        // NEWTON
        time_init();
        newton_inter_init(x, y, n);
        for(double i = from; to > i; i += step) {
            newton_inter_eval(i);
        }
        newt_time = time_diff();

        // LAGRANGE
        time_init();
        lagrange_inter_init(x, y, n);
        for(double i = from; to > i; i += step) {
            lagrange_inter_eval(i);
        }
        lagr_time = time_diff();

        // GSL
        acc = gsl_interp_accel_alloc();
        
        spline = gsl_spline_alloc(gsl_interp_polynomial, n);
        time_init();
        gsl_spline_init(spline, x, y, n);
        for (double i = from; to > i; i += step) {
            double val = gsl_spline_eval(spline, i, acc);
        }
        gsl_time = time_diff();
        gsl_spline_free(spline);
        gsl_interp_accel_free(acc);

        fprintf(lagr_times, "%f %f\n", (double)n, lagr_time);
        fprintf(newt_times, "%f %f\n", (double)n, newt_time);
        fprintf(gsl_times, "%f %f\n", (double)n, gsl_time);

        free(x);
        free(y);
    }


    fclose(lagr_times);
    fclose(gsl_times);
    fclose(newt_times);

}