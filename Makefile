CC=gcc
GSL_HOME=/usr/local
CFLAGS=-I. -I$(GSL_HOME) -std=c99
LFLAGS=-L$(GSL_HOME) -lm -lgsl -lgslcblas 



%.o: %.c %.h
	$(CC) -c -o $@ $< $(CFLAGS)

main: lagrange_inter.o newton_inter.o inter_test.o main.o
	$(CC) -o $@ $^ $(CFLAGS) $(LFLAGS)

clean: 
	rm -f *.o *.txt main *.png *.txt

.PHONY: clean