#ifndef _NEWTON_INTER_H_
#define _NEWTON_INTER_H_ 

// polynomial max N-order
#define N 10000

struct newton_info {
    double diff_table[N][N];
    double* x;
    double* y;
    double n;
} newt;


void _gen_diff_table();

void newton_inter_init(double* x, double* y, int n);

void newton_inter_print();

double newton_inter_eval(double x);

#endif