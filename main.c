#include <stdio.h>
#include <newton_inter.h>
#include <lagrange_inter.h>
#include <stdlib.h>
#include <time.h>
#include <inter_test.h>
#include <math.h>



int main(int argc, char const *argv[]) {
    srand(time(NULL));


    // 3.COMPARE 
    int n = 20;
    double from_x, to_x;
    int n_samples = 1000;

    from_x = 0.0;
    to_x = 20.0;
    double* x = malloc(n * sizeof(double));
    double* y = malloc(n * sizeof(double));

    sample_f(sin, x, y, n, from_x, to_x);
    gen_inter_file("newt_inter.txt", "newt_points.txt", x, y, n, n_samples, INTER_NEWTON, from_x, to_x);
    gen_inter_file("lagrange_inter.txt", "lagrange_points.txt", x, y, n, n_samples, INTER_LAGRANGE, from_x, to_x);
    gen_inter_file("gsl_inter.txt", "gsl_points.txt", x, y, n, n_samples, INTER_GSL, from_x, to_x);


    free(x);
    free(y);


    // 4.TIMING
    gen_times_interp();

    gen_times_eval();

    return 0;
}