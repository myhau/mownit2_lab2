#include <lagrange_inter.h>
#include <stdio.h>


void lagrange_inter_init(double* x, double* y, int n) {
  lagr.x = x;
  lagr.y = y;
  lagr.n = n;
  _compute_coefficients();
}

void _compute_coefficients() {
    for(int i = 0; lagr.n > i; i++) {
        lagr.coeff[i] = 1.0;
        for(int j = 0; lagr.n > j; j++) {
            if(i != j) {
                lagr.coeff[i] *= lagr.x[i] - lagr.x[j];
            }
        }
        lagr.coeff[i] = lagr.y[i] / lagr.coeff[i];
    }
}

void lagrange_inter_print() {
    double pre_sum;
    for(int i = 0; lagr.n > i; i++) {
        if(i != 0) printf(" + ");
        printf("%f", lagr.coeff[i]);
        for(int j = 0; lagr.n > j; j++) {
            if(i != j) {
                printf("(x - %f)", lagr.x[j]);
            }
        }
    }
    printf("\n");
}

double lagrange_inter_eval(double x) {
    double sum = 0.0;
    double pre_sum;

    for(int i = 0; lagr.n > i; i++) {
        pre_sum = 1.0;
        for(int j = 0; lagr.n > j; j++) {
            if(i != j) {
                pre_sum *= (x - lagr.x[j]);
            }
        }
        sum += pre_sum * lagr.coeff[i];
    }

    return sum;
}