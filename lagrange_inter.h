#ifndef _LAGRANGE_INTER_
#define _LAGRANGE_INTER_

#define N 10000

struct lagrange_info {
    double* x;
    double* y;
    int n;
    double coeff[N];
} lagr;

void lagrange_inter_init(double* x, double* y, int n);

void lagrange_inter_print();

double lagrange_inter_eval(double x);

void _compute_coefficients();

#endif